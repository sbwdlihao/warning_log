<?php
/**
 * Created by PhpStorm.
 * User: sbwdlihao
 * Date: 12/29/15
 * Time: 3:40 PM
 */
namespace Numbull\Warning\Log;

require_once __DIR__.'/../vendor/autoload.php';

use Numbull\Warning\Plugin\PluginImp;
use phpseclib\Net\SSH2;

class Plugin extends PluginImp{

    public function __construct($id = 0)
    {
        parent::__construct($id);
    }

    public function getType()
    {
        return 'log';
    }

    public function getName()
    {
        return 'warning_log';
    }

    public function getDescription()
    {
        return '监控日志';
    }

    public function showConfig()
    {
        return [
            'host'=>'',
            'port'=>'22',
            'username'=>'',
            'password'=>"如果密码中出现单引号'必须使用\\\\'",
            'log_path'=>'',
            'log_size'=>'',
            'line_pattern'=>'比如Page-load-timeout,pattern中间不能有空格',
        ];
    }

    public function createCrontabParams($config)
    {
        $config['store_file'] = __DIR__.'/store/'.date('YmdHis');
        return parent::createCrontabParams($config);
    }

    public function action($params)
    {
        if (count($params) != 8) {
            $this->_showResult(201, json_encode($params));
        }
        $host = $params[0];
        $port = $params[1];
        $username = $params[2];
        $password = $params[3];
        $log_path = $params[4];
        $log_size = (int)$params[5];
        $line_pattern = $params[6];
        $store_file = $params[7];

        $log_file = sprintf('log-%s.php', date('Y-m-d'));

        $ssh = new SSH2($host, $port);
        if (!$ssh->login($username, $password)) {
            $this->_showResult(202, json_encode($params));
        }

        try {
            $cd = 'cd '.$log_path;
            $size = $ssh->exec(sprintf('%s;ls -l %s | awk \'{ print $5 }\'', $cd, $log_file));
            $size = trim($size);
            if (is_numeric($size)) {
                $size = (int)$size;
                if ($size > $log_size) {
                    $this->_showResult(301, sprintf('%s size = %s over limit = %s', $log_file, $size, $log_size));
                }
            }
            $last_log = $ssh->exec(sprintf('%s;grep \'%s\' %s | tail -n 1', $cd, $line_pattern, $log_file));
            if (!empty($last_log) && preg_match(sprintf('/%s/', $line_pattern), $last_log) === 1) {
                $last_log = trim($last_log);
                $dir = dirname($store_file);
                if (!is_dir($dir)) {
                    mkdir($dir);
                }
                $old_last_log = @file_get_contents($store_file);
                if ($old_last_log != $last_log) {
                    @file_put_contents($store_file, $last_log);
                    $this->_showResult(302, $last_log);
                }
            }
        } catch (\Exception $e) {
            $this->_showResult(203, $e->getMessage());
        }
        parent::action($params);
    }
}