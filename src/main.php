<?php
/**
 * Created by PhpStorm.
 * User: sbwdlihao
 * Date: 12/28/15
 * Time: 10:16 AM
 */

namespace Numbull\Warning\Log;

require_once 'Plugin.php';

$argv = $_SERVER['argv'];
$id = $argv[1];
$params = array_slice($argv, 2);
$plugin = new Plugin($id);
$plugin->action($params);